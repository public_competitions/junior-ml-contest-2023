import logging
import os
import warnings

import click
import mlflow.pyfunc
import numpy as np
from dotenv import load_dotenv
from sentence_transformers import SentenceTransformer

from src.utils import pickle_load

warnings.filterwarnings("ignore")


@click.command()
@click.argument("scaler_path", type=click.Path())
@click.argument("pca_path", type=click.Path())
@click.argument("factor_analysis_path", type=click.Path())
@click.argument("lda_path", type=click.Path())
@click.argument("isomap_path", type=click.Path())
def extend_mlflow_model(scaler_path: str, pca_path: str, factor_analysis_path: str, lda_path: str, isomap_path: str):
    """
    Данная функция запускается после catboost_tuning.py, она регистрирует в mlflow новую модель, которая содержит
    в себе последнюю зарегистрированную catboost модель с именем "tuned_catboost" (stage='None'), а также функцию
    preprocess_input, которая позволяет обработать "сырой" датафрейм, содержащий в себе только столбец с текстом,
    перед его подачей в catboost.

    :param scaler_path: путь до сохраненного объекта MinMaxScaler
    :param pca_path: путь до сохраненного объекта PCA
    :param factor_analysis_path: путь до сохраненного объекта FactorAnalysis
    :param lda_path: путь до сохраненного объекта LinearDiscriminantAnalysis
    :param isomap_path: путь до сохраненного объекта Isomap
    """
    load_dotenv()
    mlflow.set_experiment('extend_catboost_model')
    remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
    mlflow.set_tracking_uri(remote_server_uri)

    with mlflow.start_run():
        logging.getLogger("mlflow").setLevel(logging.DEBUG)

        scaler = pickle_load(scaler_path)
        pca = pickle_load(pca_path)
        factor_analysis = pickle_load(factor_analysis_path)
        lda = pickle_load(lda_path)
        isomap = pickle_load(isomap_path)

        # загружаем последнюю модель catboost, сохраненную в model registry через catboost_tuning.py
        latest_staging_model_path = f"models:/tuned_catboost/None"
        model = mlflow.catboost.load_model(latest_staging_model_path)

        custom_model = CustomModel(model, scaler, pca, factor_analysis, lda, isomap)

        model_path = "extended_catboost"
        mlflow.pyfunc.log_model(
            artifact_path=model_path,
            python_model=custom_model,
            registered_model_name="extended_catboost",
        )


class CustomModel(mlflow.pyfunc.PythonModel):
    def __init__(self, model, scaler, pca, factor_analysis, lda, isomap):

        self.feature_extractor1 = SentenceTransformer('sentence-transformers/distiluse-base-multilingual-cased-v1')
        self.feature_extractor2 = SentenceTransformer('sentence-transformers/paraphrase-multilingual-mpnet-base-v2')
        self.scaler = scaler
        self.pca = pca
        self.factor_analysis = factor_analysis
        self.lda = lda
        self.isomap = isomap
        self.model = model

    def preprocess_input(self, df):
        df['text'] = df['text'].str.replace('\n', ' ')
        fe1_emb = self.feature_extractor1.encode(df['text'])
        fe2_emb = self.feature_extractor2.encode(df['text'])

        neuro_features = np.concatenate((fe1_emb, fe2_emb), axis=1)
        neuro_features = self.scaler.transform(neuro_features)

        X_pca = self.pca.transform(neuro_features)
        pca_columns = [f'PCA_{i}' for i in range(X_pca.shape[1])]

        X_fa = self.factor_analysis.transform(neuro_features)
        fa_columns = [f'FA_{i}' for i in range(X_fa.shape[1])]

        X_lda = self.lda.transform(neuro_features)
        lda_columns = [f'LDA_{i}' for i in range(X_lda.shape[1])]

        X_isomap = self.isomap.transform(neuro_features)
        isomap_columns = [f'Isomap_{i}' for i in range(X_isomap.shape[1])]

        df[pca_columns] = X_pca
        df[fa_columns] = X_fa
        df[lda_columns] = X_lda
        df[isomap_columns] = X_isomap
        return df

    def predict(self, context, input_df):
        preprocessed_df = self.preprocess_input(input_df)
        predictions = self.model.predict_proba(preprocessed_df)[:, 1]

        return predictions


if __name__ == '__main__':
    extend_mlflow_model()
