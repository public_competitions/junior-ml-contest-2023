import click
import pandas as pd
from sklearn.decomposition import PCA, FactorAnalysis
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.manifold import Isomap
from sklearn.preprocessing import MinMaxScaler

from src.utils import fit_n_pickle_save


@click.command()
@click.argument("train_df_path", type=click.Path())
@click.argument("scaler_path", type=click.Path())
@click.argument("pca_path", type=click.Path())
@click.argument("factor_analysis_path", type=click.Path())
@click.argument("lda_path", type=click.Path())
@click.argument("isomap_path", type=click.Path())
def fit_scaler_pca_fa_lda_isomap(train_df_path: str, scaler_path: str, pca_path: str, factor_analysis_path: str, lda_path: str, isomap_path: str):
    """
    Данная функция позволяет обучить MinMaxScaler, PCA, FactorAnalysis, LinearDiscriminantAnalysis и Isomap из sklearn
    для уменьшения размерности признакового пространства эмбеддингов и сохранить их в pickle формате

    :param train_df_path: путь до обучающего датафрейма
    :param scaler_path: куда сохранить объект MinMaxScaler
    :param pca_path: куда сохранить объект PCA
    :param factor_analysis_path: куда сохранить объект FactorAnalysis
    :param lda_path: куда сохранить объект LinearDiscriminantAnalysis
    :param isomap_path: куда сохранить объект Isomap
    """
    df = pd.read_csv(train_df_path)
    neuro_features = df.drop(['text', 'target'], axis=1)

    scaler = MinMaxScaler()
    scaler = fit_n_pickle_save(scaler, neuro_features, scaler_path)
    neuro_features = scaler.transform(neuro_features)

    pca = PCA(n_components=15, random_state=42)
    _ = fit_n_pickle_save(pca, neuro_features, pca_path)

    factor_analysis = FactorAnalysis(n_components=15, random_state=42)
    _ = fit_n_pickle_save(factor_analysis, neuro_features, factor_analysis_path)

    lda = LinearDiscriminantAnalysis()
    _ = fit_n_pickle_save(lda, neuro_features, lda_path)

    isomap = Isomap(n_components=5)
    _ = fit_n_pickle_save(isomap, neuro_features, isomap_path)


if __name__ == "__main__":
    fit_scaler_pca_fa_lda_isomap()
