'''
Данный модуль предназначен для инференса итоговой модели через телеграм бота.
Модель подгружается через mlflow model registry.
'''
import logging
import os
import random
import warnings
from pathlib import Path

import mlflow
import numpy as np
import pandas as pd
import telebot
from dotenv import load_dotenv
from telebot import types

WELCOME_MESSAGE = "Привет!\nЯ - бот, написанный для участия в конкурсе Junior ML Contest 2023. Я не волшебник, " \
               "пока только учусь, но постараюсь определить, написан текст человеком или ИИ-моделью. Попробуй " \
               "написать мне сообщение, а я угадаю, кем был написан текст :)"

warnings.filterwarnings("ignore")

load_dotenv()

remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(remote_server_uri)

telebot_token = os.getenv("TELEBOT_TOKEN")
bot = telebot.TeleBot(telebot_token)

latest_staging_model_path = f"models:/extended_catboost/None"
loaded_model = mlflow.pyfunc.load_model(latest_staging_model_path)
logging.warning('Модель загружена')


@bot.message_handler(commands=['start'])
def welcome(message):
    """
    Функция для обработки команды /start. Отправляет приветственное сообщение
    :param message: объект сообщения от пользователя
    """
    sti = open(str(Path('./stickers/prediction.webp')), 'rb')
    bot.send_sticker(message.chat.id, sti)
    bot.send_message(message.chat.id, WELCOME_MESSAGE, parse_mode='html')


@bot.message_handler(content_types=['text'])
def predict_response(message):
    """
    Обрабатывает текстовое сообщение от пользователя - предсказывает вероятность того, что текст был написан ИИ и
    отпрвляет предсказание пользователю. После получения ответа у пользователя есть возможность с помощью кнопок выбрать,
    был ли верным ответ бота

    :param message: объект сообщения от пользователя
    """
    sti = open(str(Path('./stickers/hmm.webp')), 'rb')
    bot.send_sticker(message.chat.id, sti)

    df = pd.DataFrame()
    df['text'] = [message.text]

    pred_proba = loaded_model.predict(df)[0]

    messages = [f'Я думаю, что вероятность написания этого текста ИИ составляет {pred_proba:.2f}',
                f'Имхо вероятность написания сего текста машиной - {pred_proba:.2f}',
                f'Вероятность того, что этот текст не был написан человеком - {pred_proba:.2f}']

    bot.send_message(message.chat.id, random.choice(messages), reply_markup=get_buttons())


def get_buttons():
    """
    Вспомогательная функция для predict_response. Создает кнопки для сообщения бота.
    :return: объект InlineKeyboardMarkup, содержащий в себе кнопки
    """
    buttons = types.InlineKeyboardMarkup(row_width=2)
    button1 = types.InlineKeyboardButton('Верно ✅', callback_data='correct')
    button2 = types.InlineKeyboardButton('Неверно ❌', callback_data='incorrect')
    buttons.add(button1, button2)
    return buttons


def inline_response(call, sticker1_path: str, sticker2_path: str, response_text: str):
    """
    Вспомогательная функция для callback_inline. Формирует ответ на нажатие кнопки.
    :param call: объект со сведениями о вызове функции callback_inline
    :param sticker1_path: путь до первого случайного стикера
    :param sticker2_path: путь до второго случайного стикера
    :param response_text: текст ответа
    """
    if np.random.random() > 0.5:
        sti = open(str(Path(sticker1_path)), 'rb')
    else:
        sti = open(str(Path(sticker2_path)), 'rb')
    bot.send_sticker(call.message.chat.id, sti)
    bot.send_message(call.message.chat.id, response_text)


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    """
    Обработчик нажатия кнопок "Верно" и "Неверно". Отправляет в ответ пользователю сообщение.
    :param call: объект со сведениями о вызове функции callback_inline
    """
    if not call.message:
        return

    if call.data == 'correct':
        inline_response(call, './stickers/congrats.tgs', './stickers/success.tgs',
                        'Вот и хорошо 😊\nПопробуй написать ещё что-нибудь!')
    elif call.data == 'incorrect':
        inline_response(call, './stickers/overfit.webp', './stickers/wa.webp',
                        'Я обязательно научусь, а пока попробуй ещё')

    # remove inline buttons
    bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                          text=call.message.text, reply_markup=None)


@bot.message_handler(content_types=['audio', 'photo', 'voice', 'video', 'document', 'location', 'contact', 'sticker'])
def trouble(message):
    """
    Обрабатывает все остальные типы сообщений - отправляет в ответ сообщение о том, что бот работает только с текстом
    :param message: объект сообщения от пользователя
    """
    sti = open(str(Path('./stickers/беда.webp')), 'rb')
    bot.send_sticker(message.chat.id, sti)
    bot.send_message(message.chat.id, "Я умею понимать только текст, попробуй ещё раз")


bot.polling(none_stop=True)  # запуск бота
