import numpy as np
import pickle

from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix, log_loss, accuracy_score, precision_score, recall_score, roc_auc_score, \
    f1_score
import seaborn as sns


def reduce_mem_usage(df):
    """
    Уменьшает размер датафрейма за счёт оптимизации типов для int и float столбцов
    :param df: входной датафрейм
    :return df: оптимизированный датафрейм
    """
    for col in df.columns:
        col_type = df[col].dtype.name

        if col_type not in ['object', 'category', 'datetime64[ns, UTC]']:
            c_min = df[col].min()
            c_max = df[col].max()
            if str(col_type)[:3] == 'int':
                if c_min > np.iinfo(np.int8).min and c_max < np.iinfo(np.int8).max:
                    df[col] = df[col].astype(np.int8)
                elif c_min > np.iinfo(np.int16).min and c_max < np.iinfo(np.int16).max:
                    df[col] = df[col].astype(np.int16)
                elif c_min > np.iinfo(np.int32).min and c_max < np.iinfo(np.int32).max:
                    df[col] = df[col].astype(np.int32)
                elif c_min > np.iinfo(np.int64).min and c_max < np.iinfo(np.int64).max:
                    df[col] = df[col].astype(np.int64)
            else:
                if c_min > np.finfo(np.float16).min and c_max < np.finfo(np.float16).max:
                    df[col] = df[col].astype(np.float16)
                elif c_min > np.finfo(np.float32).min and c_max < np.finfo(np.float32).max:
                    df[col] = df[col].astype(np.float32)
                else:
                    df[col] = df[col].astype(np.float64)

    return df


def pickle_load(path_to_object):
    """
    Вспомогательная функция для загрузки сохраненных pickle объектов
    :param path_to_object: путь до сохраненного объекта
    :return: загруженный объект
    """
    with open(path_to_object, 'rb') as fp:
        loaded_object = pickle.load(fp)
        return loaded_object


def fit_n_pickle_save(model, fit_data, path_to_save):
    """
    Вспомогательная функция для обучения и сохранения моделей для преобразования фичей
    (MinMaxScaler, PCA, FactorAnalysis, LinearDiscriminantAnalysis и Isomap)

    :param model: изначальная модель
    :param fit_data: данные для обучения
    :param path_to_save: куда сохранить модель
    :return: обученная модель
    """
    model.fit(fit_data)
    with open(path_to_save, 'wb') as file:
        pickle.dump(model, file)
    return model


def pickle_load_transform(path_to_object, data_to_transform, model_name):
    """
    Загружает сохраненную pickle модель, осуществляет transform данных,
    а также формирует имена столбцов для датафрейма
    :param path_to_object: путь до сохраненной модели
    :param data_to_transform: для каких данных осуществить transform
    :param model_name: имя модели для столбцов
    :return: преобразованные данные, имена столбцов для df
    """
    loaded_object = pickle_load(path_to_object)
    transformed_data = loaded_object.transform(data_to_transform)
    transformed_data_cols = [f'{model_name}_{i}' for i in range(transformed_data.shape[1])]
    return transformed_data, transformed_data_cols


def get_confusion_matrix(targets, preds):
    """
    Строит нормированный confusion matrix, возвращает fig для дальнейшего логирования в mlflow
    :param targets: истинные ответы
    :param preds: предсказания модели
    :return:
    """
    cf_matrix = confusion_matrix(targets, preds)
    fig, ax = plt.subplots(dpi=100)
    sns.heatmap(cf_matrix / np.sum(cf_matrix), annot=True, fmt='.2%', cmap='Blues')
    ax.set_title('Test Confusion Matrix \n')
    ax.set_xlabel('\nPredicted Values')
    ax.set_ylabel('Actual Values ')
    ax.xaxis.set_ticklabels(['False', 'True'])
    ax.yaxis.set_ticklabels(['False', 'True'])
    return fig


def catboost_calc_metrics(catboost_classifier, data, targets):
    """
    Вспомогательная функция, считает метрики классификации
    :param catboost_classifier: CatBoostClassifier
    :param data: данные в модель
    :param targets: истинные ответы
    :return: предсказания и метрики в виде словаря
    """
    proba = catboost_classifier.predict_proba(data)[:, 1]
    preds = catboost_classifier.predict(data)

    metrics = dict(
        train_logloss=log_loss(targets, proba),
        train_acc=accuracy_score(targets, preds),
        train_precision=precision_score(targets, preds),
        train_recall=recall_score(targets, preds),
        train_rocauc=roc_auc_score(targets, proba),
        train_f1=f1_score(targets, preds),
    )
    return preds, metrics


def get_good_catboost_params():
    """
    Возвращает список с хорошими гиперпараметрами, найденными optuna.
    :return: список параметров в виде словарей
    """
    good_params1 = {
        'learning_rate': 0.48684276021295453,
        'depth': 4,
        'l2_leaf_reg': 1.4792157364496645,
        'random_strength': 0.9532915548981323,
        'bootstrap_type': 'Bernoulli',
        'min_data_in_leaf': 7,
        'subsample': 0.9199895576044482
    }

    good_params2 = {
        'learning_rate': 0.44688251015940267,
        'depth': 4,
        'l2_leaf_reg': 1.3720330267414245,
        'random_strength': 0.7058520226446663,
        'bootstrap_type': 'Bernoulli',
        'min_data_in_leaf': 10,
        'subsample': 0.972912634760406
    }

    good_params3 = {
        'learning_rate': 0.4641391497437345,
        'depth': 4,
        'l2_leaf_reg': 2.002161877048938,
        'random_strength': 1.3070019538683202,
        'bootstrap_type': 'Bernoulli',
        'min_data_in_leaf': 20,
        'subsample': 0.6778161537149421
    }

    return [good_params1, good_params2, good_params3]
