import click
import pandas as pd
import pickle

from src.utils import pickle_load_transform


@click.command()
@click.argument("input_df_path", type=click.Path())
@click.argument("output_df_path", type=click.Path())
@click.argument("scaler_path", type=click.Path())
@click.argument("pca_path", type=click.Path())
@click.argument("factor_analysis_path", type=click.Path())
@click.argument("lda_path", type=click.Path())
@click.argument("isomap_path", type=click.Path())
def scaler_pca_fa_lda_isomap_transform(input_df_path: str, output_df_path: str, scaler_path: str, pca_path: str,
                                       factor_analysis_path: str, lda_path: str, isomap_path: str):
    """
    В этой функции сначала осуществляет скейлинг эмбеддингов (scaler - MinMaxScaler),
    затем эти эмбеддинги трансформируются с помощью PCA, FactorAnalysis, LinearDiscriminantAnalysis и Isomap из sklearn
    для уменьшения признакового пространства и повышения разнообразия природы признаков. Исходные эмбеддинги в
    результирующий датафрейм не входят, т.к. их линейные комбинации уже заложены в PCA-фичи, а также эксперименты
    показали, что исходные эмбеддинги  показывают себя хуже чем эти фичи.
    :param input_df_path: путь до входного датафрейма
    :param output_df_path: куда сохранить результирующий датафрейм
    :param scaler_path: путь к сохраненному объекту MinMaxScaler
    :param pca_path: путь к сохраненному объекту PCA
    :param factor_analysis_path: путь к сохраненному объекту FactorAnalysis
    :param lda_path: путь к сохраненному объекту LinearDiscriminantAnalysis
    :param isomap_path: путь к сохраненному объекту Isomap
    """
    df = pd.read_csv(input_df_path)
    neuro_features = df.drop(['text', 'target'], axis=1)

    neuro_features, _ = pickle_load_transform(scaler_path, neuro_features, 'MinMaxScaler')
    X_pca, pca_columns = pickle_load_transform(pca_path, neuro_features, 'PCA')
    X_fa, fa_columns = pickle_load_transform(factor_analysis_path, neuro_features, 'FA')
    X_lda, lda_columns = pickle_load_transform(lda_path, neuro_features, 'LDA')
    X_isomap, isomap_columns = pickle_load_transform(isomap_path, neuro_features, 'Isomap')

    output_df = pd.DataFrame()
    output_df['text'] = df['text']
    output_df['target'] = df['target']
    output_df[pca_columns] = X_pca
    output_df[fa_columns] = X_fa
    output_df[lda_columns] = X_lda
    output_df[isomap_columns] = X_isomap

    output_df.to_csv(output_df_path, index=False)


if __name__ == "__main__":
    scaler_pca_fa_lda_isomap_transform()
