import click
import pandas as pd
from sentence_transformers import SentenceTransformer

from src.utils import reduce_mem_usage


@click.command()
@click.argument("input_df_path", type=click.Path())
@click.argument("output_df_path", type=click.Path())
def add_embeddings(input_df_path: str, output_df_path: str):
    """
    Данная функция с помощью двух трансформеров (мультиязычных) вычисляет эмбеддинги для текстов в датафрейме,
    в результирующем датафрйеме останется текст + добавятся фичи-эмбеддинги
    :param input_df_path: путь входного датафрейма
    :param output_df_path: куда сохранить результирующий датафрейм
    """
    df = pd.read_csv(input_df_path)
    X_text = df['text']

    feature_extractor1 = SentenceTransformer('sentence-transformers/distiluse-base-multilingual-cased-v1')
    feature_extractor2 = SentenceTransformer('sentence-transformers/paraphrase-multilingual-mpnet-base-v2')

    fe1_emb = feature_extractor1.encode(X_text.tolist())
    fe2_emb = feature_extractor2.encode(X_text.tolist())

    fe1_cols = [f'FE1_{i}' for i in range(fe1_emb.shape[1])]
    fe2_cols = [f'FE2_{i}' for i in range(fe2_emb.shape[1])]

    df[fe1_cols] = fe1_emb
    df[fe2_cols] = fe2_emb

    df = reduce_mem_usage(df)
    df.to_csv(output_df_path, index=False)


if __name__ == "__main__":
    add_embeddings()
