import click
import pandas as pd
from sklearn.model_selection import train_test_split

from src.utils import reduce_mem_usage


@click.command()
@click.argument("input_df_path", type=click.Path())
@click.argument("output_train_path", type=click.Path())
@click.argument("output_val_path", type=click.Path())
@click.argument("output_test_path", type=click.Path())
def train_val_test_split(input_df_path: str, output_train_path: str, output_val_path: str, output_test_path: str):
    """
    Делит датафрейм на входе на тренировочный, валидационный и тестовый датафреймы
    :param input_df_path: путь к входному датафрейму
    :param output_train_path: куда сохранить тренировочный датафрейм
    :param output_val_path: куда сохранить валидационный датафрейм
    :param output_test_path: куда сохранить тестовый датафрейм
    """
    df = pd.read_csv(input_df_path)

    train_df, test_df = train_test_split(df, shuffle=True, stratify=df['target'], test_size=0.15, random_state=42)
    train_df, val_df = train_test_split(train_df, shuffle=True, stratify=train_df['target'], test_size=0.15, random_state=42)

    train_df = reduce_mem_usage(train_df)
    val_df = reduce_mem_usage(val_df)
    test_df = reduce_mem_usage(test_df)

    train_df.to_csv(output_train_path, index=False)
    val_df.to_csv(output_val_path, index=False)
    test_df.to_csv(output_test_path, index=False)


if __name__ == "__main__":
    train_val_test_split()
