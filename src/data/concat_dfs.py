import click
import numpy as np
import pandas as pd

from src.utils import reduce_mem_usage


@click.command()
@click.argument("codenrock_path", type=click.Path())
@click.argument("rucode_path", type=click.Path())
@click.argument("output_path", type=click.Path())
def concat_dfs(codenrock_path: str, rucode_path: str, output_path: str):
    """
    Объединяет 2 входных датафрейма в один
    :param codenrock_path: путь до датафрейма от codenrock
    :param rucode_path: путь до датафрейма из rucode
    :param output_path: куда сохранить итоговый датафрейм
    """
    rucode_df = pd.read_csv(rucode_path)
    codenrock_df = pd.read_csv(codenrock_path)

    codenrock_texts = codenrock_df["ans_text"].values
    codenrock_y = codenrock_df["label"].map({'ai_answer': 1, 'hu_answer': 0}).values
    rucode_texts = rucode_df["answer"].values
    rucode_y = rucode_df["label"].map({'ai': 1, 'people': 0}).values

    concatenated_texts = np.concatenate((codenrock_texts, rucode_texts), axis=0)
    concatenated_y = np.concatenate((codenrock_y, rucode_y), axis=0)

    output_df = pd.DataFrame()
    output_df['text'] = concatenated_texts
    output_df['target'] = concatenated_y

    output_df = reduce_mem_usage(output_df)
    output_df.to_csv(output_path, index=False)


if __name__ == "__main__":
    concat_dfs()
