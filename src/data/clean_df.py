import click
import pandas as pd


@click.command()
@click.argument("input_df_path", type=click.Path())
@click.argument("output_df_path", type=click.Path())
@click.argument("column_name", type=click.STRING)
def clean_df(input_df_path: str, output_df_path: str, column_name: str):
    """
    В датасетах в ответах людей замечены символы переноса строки, которых нет в ответах ИИ
    Данная функция заменяет этот символ на пробел в соответствующем столбце датафрейма
    :param input_df_path: путь до датафрейма наа входе
    :param output_df_path: куда сохранить итоговйы датафрейм
    :param column_name: имя столбца с текстом
    """
    df = pd.read_csv(input_df_path)
    df[column_name] = df[column_name].str.replace('\n', ' ')  # символ переноса строки часто встречается в ответах людей
    df.to_csv(output_df_path, index=False)


if __name__ == "__main__":
    clean_df()
